using NUnit.Framework;
using NUnit.Framework.Internal;
using SideFX.Events;

namespace SideFX.Tests {
    public readonly struct TestEvent : IEvent { }

    [TestFixture]
    public class EventBusTests {
        [Test]
        public void WhenEventsAreRaised_BindingsAreInvoked_IfRegistered() {
            var eventWasHandled = false;
            EventBinding<TestEvent> binding = new(() => eventWasHandled = true);
            EventBus<TestEvent>.Register(binding);

            EventBus<TestEvent>.Raise(new()); // Binding should be invoked
            Assert.IsTrue(eventWasHandled);

            EventBus<TestEvent>.Deregister(binding); // Unregister binding
            eventWasHandled = false;

            EventBus<TestEvent>.Raise(new()); // Binding should not be invoked
            Assert.IsFalse(eventWasHandled);
        }
    }
}
