using UnityEngine;

namespace SideFX.Anchors
{
    [CreateAssetMenu(fileName = "Transform Anchor", menuName = "SideFX/Anchors/Transform")]
    public class TransformAnchor : AnchorBase<Transform> { }
}
