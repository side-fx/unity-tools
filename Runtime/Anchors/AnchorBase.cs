using System;
using UnityEngine;

namespace SideFX.Anchors {
    /// <summary>
    /// Anchors are shared references to object components that you can place in the project!
    /// They are used as a modular way to communicate between systems,
    /// as an alternative to GameObject.Find() or similar lookups
    /// </summary>
    /// <typeparam name="T">The type of data the anchor stores</typeparam>
    public abstract class AnchorBase<T> : DescribedScriptable where T : UnityEngine.Object {
        /// <summary>
        /// Event that fires when a new reference is provided to the anchor.
        /// </summary>
        public event Action OnAnchorUpdated = delegate { };

        /// <summary>
        /// The value contained by the anchor.
        /// The backing field of the property is serialized,
        /// allowing it to be viewed in the inspector
        /// </summary>
        [field: SerializeField] public T Value { get; private set; }

        public bool IsSet { get; private set; } = false;

        /// <summary>
        /// Update the reference stored by the anchor.
        /// Will log an error and early exit if passed a null reference.
        /// </summary>
        /// <param name="value">Data to store in the anchor.</param>
        public void Provide(T value) {
            if (value is null) {
                Debug.LogError($"Null reference provided to {name} runtime anchor", this);
                return;
            }

            Value = value;
            IsSet = true;

            OnAnchorUpdated?.Invoke();
        }

        public void Clear() {
            Value = null;
            IsSet = false;
        }

        private void OnDisable() => Clear();
    }
}
