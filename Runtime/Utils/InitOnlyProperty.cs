
namespace System.Runtime.CompilerServices {
    /// <summary>
    /// This class is defined in order to enable init only properties
    /// for more info see <see href="https://forum.unity.com/threads/init-only-setter-not-working-in-2021-2.1222452/">this forum thread</see>
    /// </summary>
    public class IsExternalInit { }
}
