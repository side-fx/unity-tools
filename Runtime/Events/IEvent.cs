namespace SideFX.Events {
    /// <summary>
    /// Marker interface for event types
    /// </summary>
    public interface IEvent { }
}
