using UnityEngine;
using UnityEngine.Events;

namespace SideFX.Events {
    public abstract class EventListener<T> : MonoBehaviour {
        [SerializeField] private EventChannel<T> _eventChannel;
        [SerializeField] private UnityEvent<T> _unityEvent;

        protected void OnEnable() => _eventChannel.Register(this);
        protected void OnDisable() => _eventChannel.Deregister(this);

        /// <summary>
        /// Handles events from _eventChannel
        /// </summary>
        public void Raise(T value) => _unityEvent?.Invoke(value);
    }

    public class EventListener : EventListener<EmptyEvent> { }
}
