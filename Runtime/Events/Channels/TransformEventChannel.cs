using UnityEngine;

namespace SideFX.Events {
    [CreateAssetMenu(fileName = "TransformEventChannel", menuName = "SideFX/Events/Transform Channel", order = 3)]
    public class TransformEventChannel : EventChannel<Transform> { }
}
