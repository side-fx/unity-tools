using UnityEngine;

namespace SideFX.Events {
    [CreateAssetMenu(fileName = "Bool Event Channel", menuName = "SideFX/Events/Bool Channel")]
    public class BoolEventChannel : EventChannel<bool> { }
}
