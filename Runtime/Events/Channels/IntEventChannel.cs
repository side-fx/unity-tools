using UnityEngine;

namespace SideFX.Events {
    [CreateAssetMenu(fileName = "Event Channel", menuName = "SideFX/Events/Int Channel", order = 2)]
    public class IntEventChannel : EventChannel<int> { }
}
