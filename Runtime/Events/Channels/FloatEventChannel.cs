using UnityEngine;

namespace SideFX.Events {
    [CreateAssetMenu(fileName = "Event Channel", menuName = "SideFX/Events/Float Channel", order = 1)]
    public class FloatEventChannel : EventChannel<float> { }
}
