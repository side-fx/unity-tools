using System.Collections.Generic;
using UnityEngine;

namespace SideFX.Events {
    public abstract class EventChannel<T> : ScriptableObject {
        private readonly HashSet<EventListener<T>> _listeners = new();

        /// <summary>
        /// Add a listener to this channel
        /// </summary>
        public void Register(EventListener<T> listener) => _listeners.Add(listener);

        /// <summary>
        /// Remove a listener from this channel
        /// </summary>
        public void Deregister(EventListener<T> listener) => _listeners.Remove(listener);

        /// <summary>
        /// Send an event to all listeners
        /// </summary>
        public void Raise(T @event) {
            foreach (EventListener<T> listener in _listeners) {
                listener.Raise(@event);
            }
        }
    }

    public readonly struct EmptyEvent : IEvent { }

    [CreateAssetMenu(fileName = "Event Channel", menuName = "SideFX/Events/Event Channel", order = 0)]
    public class EventChannel : EventChannel<EmptyEvent> {
        public void Raise() => Raise(new());
    }
}
