using System.Collections.Generic;
using UnityEngine;

namespace SideFX.Events {
    public static class EventBus<T> where T : IEvent {
        private static readonly HashSet<IEventBinding<T>> _bindings = new();

        public static void Register(EventBinding<T> binding) => _bindings.Add(binding);
        public static void Deregister(EventBinding<T> binding) => _bindings.Remove(binding);

        public static void Raise(T @event) {
            foreach (IEventBinding<T> binding in _bindings) {
                // Invoke both handlers
                // typically only one will actually exist per binding
                binding.OnEvent?.Invoke(@event);
                binding.OnEventNoArgs?.Invoke();
            }
        }

        /// <summary>
        /// Called by <see cref="SideFX.Events.EventBusUtil"/> via reflection.
        /// </summary>
        private static void Clear() {
            Debug.Log($"Clearing {typeof(T).Name} bindings");
            _bindings.Clear();
        }
    }
}
