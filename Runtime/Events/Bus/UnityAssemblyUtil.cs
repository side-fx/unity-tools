using System;
using System.Collections.Generic;
using System.Reflection;

namespace SideFX.Events {
    /// <summary>
    /// A utility class providing methods to interact with Unity's predefined assemblies.
    /// It allows to get all types in the current AppDomain that implement a specific Interface type.
    /// </summary>
    internal static class UnityAssemblyUtil {

        /// <summary>
        /// Adds all types that implement the specified interface to the provided collection.
        /// </summary>
        /// <param name="assemblyTypes">Array of Type objects representing all the types in the assembly.</param>
        /// <param name="interfaceType">Type representing the interface to be checked against.</param>
        /// <param name="results">Collection of types where result should be added.</param>
        private static void AddTypesFromAssembly(Type[] assemblyTypes, Type interfaceType, ICollection<Type> results) {
            if (assemblyTypes is null) return;

            for (var i = 0; i < assemblyTypes.Length; i++) {
                Type type = assemblyTypes[i];
                if (type != interfaceType && interfaceType.IsAssignableFrom(type))
                    results.Add(type);
            }
        }

        /// <summary>
        /// Gets all Types from all assemblies in the current AppDomain that implement the provided interface type.
        /// </summary>
        /// <param name="interfaceType">Interface type to get all the Types for.</param>
        /// <returns>List of Types implementing the provided interface type.</returns>
        public static List<Type> GetImplementors(Type interfaceType) {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<Type> types = new();

            for (var i = 0; i < assemblies.Length; i++)
                AddTypesFromAssembly(assemblies[i].GetTypes(), interfaceType, types);

            return types;
        }
    }
}
