using System;

namespace SideFX.Events {
    internal interface IEventBinding<T> where T : IEvent {
        public Action<T> OnEvent { get; set; }
        public Action OnEventNoArgs { get; set; }
    }

    /// <summary>
    /// Represents a binding between events from the <see cref="SideFX.Events.EventBus{T}"/>
    /// and a set of handlers.
    /// </summary>
    public class EventBinding<T> : IEventBinding<T> where T : IEvent {
        private Action<T> _onEvent = delegate { };
        private Action _onEventNoArgs = delegate { };

        Action<T> IEventBinding<T>.OnEvent {
            get => _onEvent;
            set => _onEvent = value;
        }

        Action IEventBinding<T>.OnEventNoArgs {
            get => _onEventNoArgs;
            set => _onEventNoArgs = value;
        }

        public EventBinding(Action<T> onEvent) => _onEvent = onEvent;
        public EventBinding(Action onEventNoArgs) => _onEventNoArgs = onEventNoArgs;

        public void Add(Action<T> onEvent) => _onEvent += onEvent;
        public void Remove(Action<T> onEvent) => _onEvent -= onEvent;

        public void Add(Action onEvent) => _onEventNoArgs += onEvent;
        public void Remove(Action onEvent) => _onEventNoArgs -= onEvent;
    }
}
