using UnityEngine;

namespace SideFX {
    public abstract class DescribedScriptable : SerializedScriptable {
        [SerializeField, TextArea] private string _description;
    }
}
