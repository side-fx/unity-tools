using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SideFX {
    public abstract class SerializedScriptable : ScriptableObject {
        public string Guid { get; private set; }
#if UNITY_EDITOR
        private void OnValidate() {
            string path = AssetDatabase.GetAssetPath(this);
            string guid = AssetDatabase.AssetPathToGUID(path);
            if (string.IsNullOrEmpty(guid)) {
                Debug.LogError("Unable to look up asset guid", this);
                return;
            }
            Guid = guid;
        }
#endif
    }
}
