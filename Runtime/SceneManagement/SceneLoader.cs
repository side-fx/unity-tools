using System.Collections;
using SideFX.Events;
using SideFX.SceneManagement.Events;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace SideFX.SceneManagement {
    public sealed class SceneLoader : MonoBehaviour {
        [SerializeField] private GameplayManagersScene _gameplayManagersScene;

        private EventBinding<LoadRequest> _loadRequestBinding;
        private AsyncOperationHandle<SceneInstance> _loadingOperationHandle;
        private AsyncOperationHandle<SceneInstance> _gameplayManagerLoadingOpHandle;
        private SceneInstance _gameplayManagerSceneInstance = new();
        private SceneData _currentlyLoadedScene;
        private SceneData _sceneToLoad;
        private bool _showLoadingScreen;
        private float _fadeDuration = 0.5f;
        private bool _isLoading = false; // Used to prevent load requests while loads are in progress

        #region Unity Callbacks

        private void OnEnable() {
            _loadRequestBinding = new EventBinding<LoadRequest>(LoadSceneData);
            EventBus<LoadRequest>.Register(_loadRequestBinding);
        }

        private void OnDisable() => EventBus<LoadRequest>.Deregister(_loadRequestBinding);

        #endregion

        private void LoadSceneData(LoadRequest request) {
            // Only load one scene at a time
            if (_isLoading) return;
            _isLoading = true;

            _sceneToLoad = request.SceneData;
            _showLoadingScreen = request.ShowLoadingScreen;

            switch (request.SceneData) {
                case MainMenuScene menu:
                    Debug.Log("Loading Main Menu");
                    // Unload gameplay managers if they're loaded
                    // e.g. when quitting to main menu from gameplay scene
                    if (_gameplayManagerSceneInstance.Scene != null && _gameplayManagerSceneInstance.Scene.isLoaded) {
                        Debug.Log($"Unloading scene: {_gameplayManagerSceneInstance.Scene}");
                        Addressables.UnloadSceneAsync(_gameplayManagerLoadingOpHandle, true);
                    }
                    StartCoroutine(UnloadPreviousScene());
                    return;
                case GameplayScene gameplay:
                    Debug.Log($"Loading gameplay scene: {request.SceneData.SceneReference}");
                    // Load gameplay managers if not already loaded
                    if (_gameplayManagerSceneInstance.Scene != null || !_gameplayManagerSceneInstance.Scene.isLoaded) {
                        Debug.Log($"Loading Gameplay Managers: {_gameplayManagersScene.SceneReference}");
                        _gameplayManagerLoadingOpHandle = _gameplayManagersScene.SceneReference.LoadSceneAsync(LoadSceneMode.Additive, true);
                        _gameplayManagerLoadingOpHandle.Completed += OnGameplayManagersLoaded;
                    } else {
                        StartCoroutine(UnloadPreviousScene());
                    }
                    return;
            }
        }

        private void OnGameplayManagersLoaded(AsyncOperationHandle<SceneInstance> handle) {
            Debug.Log("Gameplay Managers loaded");
            _gameplayManagerSceneInstance = _gameplayManagerLoadingOpHandle.Result;
            StartCoroutine(UnloadPreviousScene());
        }

        private IEnumerator UnloadPreviousScene() {
            EventBus<FadeTo>.Raise(FadeTo.Black(_fadeDuration));
            yield return new WaitForSeconds(_fadeDuration);

            if (_currentlyLoadedScene != null) { // Would be null if game was started via Initialization
                if (_currentlyLoadedScene.SceneReference.OperationHandle.IsValid()) {
                    Debug.Log($"Unloading scene: {_currentlyLoadedScene.SceneReference}");
                    _currentlyLoadedScene.SceneReference.UnLoadScene();
                }
#if UNITY_EDITOR
                else {
                    //Only used when, after a "cold start", the player moves to a new scene
                    //Since the AsyncOperationHandle has not been used (the scene was already open in the editor),
                    //the scene needs to be unloaded using regular SceneManager instead of as an Addressable
                    SceneManager.UnloadSceneAsync(_currentlyLoadedScene.SceneReference.editorAsset.name);
                }
#endif
            }
            LoadNewScene();
        }

        /// <summary>
        /// Start the loading of the next scene
        /// </summary>
        private void LoadNewScene() {
            if (_showLoadingScreen) EventBus<ToggleLoadingScreen>.Raise(ToggleLoadingScreen.On);

            _loadingOperationHandle = _sceneToLoad.SceneReference.LoadSceneAsync(LoadSceneMode.Additive, true, 0);
            _loadingOperationHandle.Completed += OnNewSceneLoaded;
        }

        /// <summary>
        /// Called after a scene is loaded.
        /// Hides the loading screen, fades in the view, and starts gameplay.
        /// </summary>
        private void OnNewSceneLoaded(AsyncOperationHandle<SceneInstance> handle) {
            SceneManager.SetActiveScene(handle.Result.Scene);
            _currentlyLoadedScene = _sceneToLoad;
            _isLoading = false;

            if (_showLoadingScreen) EventBus<ToggleLoadingScreen>.Raise(ToggleLoadingScreen.Off);

            EventBus<FadeIn>.Raise(new FadeIn { Duration = _fadeDuration });
            EventBus<SceneReady>.Raise(new SceneReady(_currentlyLoadedScene));
        }
    }
}
