using SideFX.Events;
using SideFX.SceneManagement.Events;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace SideFX.SceneManagement {
    /// <summary>
    /// Responsible for starting the game by loading the game's persistent managers, and then loading the main menu
    /// </summary>
    public class InitializationLoader : MonoBehaviour {
        [SerializeField] private PersistentManagersScene _persistentManagersScene;
        [SerializeField] private MainMenuScene _mainMenuScene;

        private void Start() {
#if DEBUG
            Debug.Log($"Loading Persistent Managers Scene: {_persistentManagersScene}");
#endif
            _persistentManagersScene.SceneReference.LoadSceneAsync(LoadSceneMode.Additive, true).Completed += LoadMainMenu;
        }

        /// <summary>
        /// Called after the SceneLoader has been loaded (hehe)
        /// Sends a request to load the main menu,
        /// then unloads the bootstrapping scene (which this object should be in)
        /// </summary>
        private void LoadMainMenu(AsyncOperationHandle<SceneInstance> handle) {
            EventBus<LoadRequest>.Raise(new LoadRequest(_mainMenuScene));

            // Initialization scene should be the only scene in build settings
            // So its index should always be 0
            SceneManager.UnloadSceneAsync(0);
        }

    }
}
