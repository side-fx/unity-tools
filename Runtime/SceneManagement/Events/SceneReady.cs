using SideFX.Events;

namespace SideFX.SceneManagement.Events {
    public readonly struct SceneReady : IEvent {
        public readonly SceneData Scene { get; init; }

        public SceneReady(SceneData scene) => Scene = scene;
    }
}
