using SideFX.Events;
using UnityEngine;

namespace SideFX.SceneManagement.Events {
    /// <summary>
    /// Send this event via the EventBus to fade the screen to a particular colour
    /// </summary>
    public readonly struct FadeTo : IEvent {
        public readonly Color TargetColor { get; init; }
        public readonly float Duration { get; init; }

        public static FadeTo Black(float duration) => new() { TargetColor = Color.black, Duration = duration };
    }

    /// <summary>
    /// Send this event via the EventBus to clear the fade from the screen
    /// </summary>
    public readonly struct FadeIn : IEvent {
        public float Duration { get; init; }
    }

}
