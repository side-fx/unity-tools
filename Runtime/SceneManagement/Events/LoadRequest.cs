using SideFX.Events;

namespace SideFX.SceneManagement.Events {
    /// <summary>
    /// Used to notify the SceneLoader that a scene should be loaded
    /// </summary>
    public readonly struct LoadRequest : IEvent {
        public SceneData SceneData { get; init; }
        public bool ShowLoadingScreen { get; init; }
        public bool FadeScreen { get; init; }

        public LoadRequest(SceneData sceneData, bool showLoadingScreen = true, bool fadeScreen = true) {
            SceneData = sceneData;
            ShowLoadingScreen = showLoadingScreen;
            FadeScreen = fadeScreen;
        }
    }
}
