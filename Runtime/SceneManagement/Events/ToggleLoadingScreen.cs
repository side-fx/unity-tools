using SideFX.Events;

namespace SideFX.SceneManagement.Events {
    public readonly struct ToggleLoadingScreen : IEvent {
        public bool Show { get; init; }
        public static readonly ToggleLoadingScreen On = new() { Show = true };
        public static readonly ToggleLoadingScreen Off = new() { Show = false };
    }
}
