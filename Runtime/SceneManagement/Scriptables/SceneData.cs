using UnityEngine;
using UnityEngine.AddressableAssets;

namespace SideFX.SceneManagement {
    /// <summary>
    /// Stores a reference to a scene asset.
    /// Using Addressable assets enables simple asynchronous scene loading.
    /// </summary>
    public abstract class SceneData : DescribedScriptable {
        [field: SerializeField]
        public AssetReference SceneReference { get; private set; }
    }
}
