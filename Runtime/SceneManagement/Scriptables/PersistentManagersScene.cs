using UnityEngine;

namespace SideFX.SceneManagement
{
    /// <summary>
    /// Stores a reference to a scene containing managers which need to be loaded on game startup
    /// </summary>
    [CreateAssetMenu(fileName = "Persistent Managers Scene Data", menuName = "SideFX/Scenes/Persistent Managers", order = 1)]
    public sealed class PersistentManagersScene : SceneData { }
}
