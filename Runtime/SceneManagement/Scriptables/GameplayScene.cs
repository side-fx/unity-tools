using UnityEngine;

namespace SideFX.SceneManagement
{
    /// <summary>
    /// Flexible reference to any kind of gameplay scene
    /// </summary>
    [CreateAssetMenu(fileName = "Gameplay Scene Data", menuName = "SideFX/Scenes/Gameplay", order = 0)]
    public sealed class GameplayScene : SceneData { }
}
