using UnityEngine;

namespace SideFX.SceneManagement
{
    /// <summary>
    /// Stores a reference to the main menu scene
    /// </summary>
    [CreateAssetMenu(fileName = "Main Menu Scene Data", menuName = "SideFX/Scenes/Main Menu", order = 3)]
    public sealed class MainMenuScene : SceneData { }
}
