using UnityEngine;

namespace SideFX.SceneManagement
{
    /// <summary>
    /// Scenes contaiing managers only needed during gameplay
    /// </summary>
    [CreateAssetMenu(fileName = "Gameplay Managers Scene Data", menuName = "SideFX/Scenes/Gameplay Managers", order = 2)]
    public sealed class GameplayManagersScene : SceneData { }
}
