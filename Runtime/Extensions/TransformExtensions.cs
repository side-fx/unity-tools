using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions {
    /// <summary>
    ///Retrieves all the children of a given Transform
    /// </summary>
    /// <returns>an IEnumerable containing all the child transforms of the parent</returns>
    public static IEnumerable<Transform> Children(this Transform parent) {
        foreach (Transform child in parent) yield return child;
    }

    /// <summary>
    /// Resets position, rotation, and scale.
    /// </summary>
    public static void Reset(this Transform transform) {
        transform.position = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    /// <summary>
    /// Destroys all child gameobjects of the given transform
    /// </summary>
    public static void DestroyChildren(this Transform parent) =>
        parent.ForEveryChild(child => Object.Destroy(child.gameObject));


    /// <summary>
    /// Immediately destroys all child game objects of the given transform.
    /// </summary>
    public static void DestroyChildrenImmediate(this Transform parent) =>
        parent.ForEveryChild(child => Object.DestroyImmediate(child.gameObject));

    /// <summary>
    /// Enables all child game objects of the given transform.
    /// </summary>
    public static void EnableChildren(this Transform parent) =>
        parent.ForEveryChild(child => child.gameObject.SetActive(true));

    /// <summary>
    /// Disables all child game objects of the given transform.
    /// </summary>
    public static void DisableChildren(this Transform parent) =>
        parent.ForEveryChild(child => child.gameObject.SetActive(false));

    /// <summary>
    /// Perform an action for every child of this transform
    /// </summary>
    private static void ForEveryChild(this Transform parent, System.Action<Transform> action) {
        // Step through children in reverse order
        // in case action reparents or destroys a child
        for (var i = parent.childCount - 1; i >= 0; i--)
            action(parent.GetChild(i));
    }
}
