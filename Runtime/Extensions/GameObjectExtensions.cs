using UnityEngine;

public static class GameObjectExtensions {
    /// <summary>
    /// Gets a component of the given type from the GameObject.
    /// If no component exists, add one and return it.
    /// </summary>
    /// <typeparam name="T">The type of the component to get or add.</typeparam>
    /// <returns>The existing component of the given type, or a new one if no such component existed</returns>
    public static T GetOrAdd<T>(this GameObject gameObject) where T : Component =>
        gameObject.TryGetComponent(out T component)
            ? component
            : gameObject.AddComponent<T>();

    /// <summary>
    /// Returns the object itself if it exists, null otherwise.
    /// </summary>
    /// <remarks>
    /// This method helps differentiate between a null reference and a destroyed Unity object. Unity's "== null" check
    /// can incorrectly return true for destroyed objects, leading to misleading behaviour. The OrNull method use
    /// Unity's "null check", and if the object has been marked for destruction, it ensures an actual null reference is returned.
    /// </remarks>
    /// <typeparam name="T">The type of the object</typeparam>
    /// <param name="obj">The object being checked</param>
    /// <returns>The object itself if it exists and not destroyed, null otherwise</returns>
    public static T OrNull<T>(this T obj) where T : Object => obj ? obj : null;


    /// <summary>
    /// Destroys all children of the game object
    /// </summary>
    public static void DestroyChildren(this GameObject gameObject) =>
        gameObject.transform.DestroyChildren();

    /// <summary>
    /// Immediately destroys all children of the given GameObject.
    /// </summary>
    public static void DestroyChildrenImmediate(this GameObject gameObject) =>
        gameObject.transform.DestroyChildrenImmediate();

    /// <summary>
    /// Enables all child GameObjects associated with the given GameObject.
    /// </summary>
    public static void EnableChildren(this GameObject gameObject) =>
        gameObject.transform.EnableChildren();

    /// <summary>
    /// Disables all child GameObjects associated with the given GameObject.
    /// </summary>
    public static void DisableChildren(this GameObject gameObject) =>
        gameObject.transform.DisableChildren();

    /// <summary>
    /// Resets the GameObject's transform's position, rotation, and scale to their default values.
    /// </summary>
    public static void ResetTransform(this GameObject gameObject) =>
        gameObject.transform.Reset();
}
