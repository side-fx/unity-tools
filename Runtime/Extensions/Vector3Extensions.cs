using UnityEngine;

public static class Vector3Extensions {
    /// <summary>
    /// Returns a new Vector3 with given components replaced.
    /// All parameters are nullable with a default value of null,
    /// allowing function calls to be shortened using parameter names
    /// </summary>
    /// <example>
    /// <code>
    /// var vector = new Vector3(1f, 1f, 1f);
    /// var modified = vector.With(y: 0f);
    /// </code>
    /// </example>
    public static Vector3 With(
        this Vector3 original,
        float? x = null,
        float? y = null,
        float? z = null
    ) => new(x ?? original.x, y ?? original.y, z ?? original.z);

    /// <summary>
    /// Add to any component of the vector
    /// </summary>
    /// <example>
    /// <code>
    /// var vector = new Vector3(1f, 1f, 1f);
    /// var modified = vector.Add(z: 3f);
    /// </code>
    /// </example>
    public static Vector3 Add(
        this Vector3 original,
        float? x = null,
        float? y = null,
        float? z = null
    ) => new(original.x + (x ?? 0f), original.y + (y ?? 0f), original.z + (z ?? 0f));

    /// <summary>
    /// Returns a Boolean indicating whether the current Vector3 is in a given range from another Vector3
    /// </summary>
    /// <param name="current">The current Vector3 position</param>
    /// <param name="target">The Vector3 position to compare against</param>
    /// <param name="range">The range value to compare against</param>
    /// <returns>True if the current Vector3 is in the given range from the target Vector3, false otherwise</returns>
    public static bool InRangeOf(
        this Vector3 current,
        Vector3 target,
        float range
    ) => (current - target).sqrMagnitude <= range * range;
}
